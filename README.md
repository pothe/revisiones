# Scripts para SMTP con PHPMailer

**Ambas carpetas contienen scripts para enviar emails reales a cualquier dirección de correo electrónico**

**Carpeta**| Contenido|
------- | ----------- |
 k| Sistema SaaS con envio de emails |
 p| Sistema SaaS con varias llaves SMTP |
 o| Sistema SaaS en Orientado a Objetos |
 
### Scripts desarrollados por:
**Marco Antonio Castillo Reyes**
