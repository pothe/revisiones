<?php
/**
 * This example shows how to handle a simple contact form.
 */
//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
$msg = '';
//Don't run this unless we're handling a form submission
if (array_key_exists('email', $_POST)) {
    date_default_timezone_set('Etc/UTC');
    require 'vendor/autoload.php';
    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    //Tell PHPMailer to use SMTP - requires a local mail server
    //Faster and safer than using mail()
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'in-v3.mailjet.com';                     // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = '737292f01a3da433320672db08c3e12a';   // SMTP username
    $mail->Password = 'e7c5a241d86b3a94b3296ece922c753a';                           // SMTP password
    $mail->Port = 587;
    //Use a fixed address in your own domain as the from address
    //**DO NOT** use the submitter's address here as it will be forgery
    //and will cause your messages to fail SPF checks
    $mail->setFrom('arco@nuve.ml', 'Marco A.');
    //Send the message to yourself, or whoever should receive contact for submissions
    $mail->addAddress('to@yomar.co', 'Reyes C.');
    //Put the submitter's address in a reply-to header
    //This will fail if the address provided is invalid,
    //in which case we should ignore the whole request
    if ($mail->addReplyTo($_POST['email'], $_POST['name'])) {
        $mail->Subject = 'PHPMailer formulario de contacto';
        //Keep it simple - don't use HTML
        $mail->isHTML(false);
        //Build a simple message body
        $mail->Body = <<<EOT
Email: {$_POST['email']}
Nombre: {$_POST['name']}
Mensaje: {$_POST['message']}
EOT;
        //Send the message, check for errors
        if (!$mail->send()) {
            //The reason for failing to send will be in $mail->ErrorInfo
            //but you shouldn't display errors to users - process the error, log it on your server.
            $msg = 'Lo sentimos, algo salió mal. Intenta más tarde.';
            echo 'Error: ' . $mail->ErrorInfo;
        } else {
            $msg = 'Mensaje enviado! Gracias por hablarnos.';
        }
    } else {
        $msg = 'Dirección de correo invalida, mensage ignorado.';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulario de Contácto</title>
</head>
<body>
<h1>Contáctanos</h1>
<?php if (!empty($msg)) {
    echo "<h2>$msg</h2>";
} ?>
<form method="POST">
    <label for="name">Nombre: <input type="text" name="name" id="name"></label><br>
    <label for="email">Email: <input type="email" name="email" id="email"></label><br>
    <label for="message">Mensaje: <textarea name="message" id="message" rows="8" cols="20"></textarea></label><br>
    <input type="submit" value="Enviar">
</form>
</body>
</html>