# ReReIn (Registro, Recuperación e Ingreso de Usuarios)

**Script para registrar, recuperar contraseñas e inicio de sesión**

## Este script envia correos de verdad a los emails que se registren o recuperen contraseña


**ARCHIVOS DE INGRESO:**

**Archivo**| Descripción del archivo
------ | -------- 
i.php  | Archivo para ingresar con un login 
i1.php | Archivo del tablero del SaaS 
i2.php | Archivo que destruye la sesión 
i3.php | Archivo para modificar datos de usuario 

**ARCHIVOS DE RECUPERACIÓN:**

**Archivo**| Descripción del archivo
------ | -------- 
rec.html      | Archivo con formulario para solicitar contraseña 
recu.php      | Archivo que muestra mensaje y envia el email     
recumail.php  | Archivo con la plantilla del email a enviar      

**ARCHIVOS DE REGISTRO:**

**Archivo**| Descripción del archivo
------ | -------- 
reg.html      | Archivo con formulario del registro            
regi.php      | Archivo que confirma los datos                 
regis.php     | Archivo para registar y proceder al pago       
regismail.php | Archivo con plantilla email de datos de acceso 

### Se debe de cambiar los datos de acceso a la base de datos e importar archivo "basedatos.sql"

## SCRIPTS FALTANTES:
**Envio de email con confirmación para enviar el email con las credenciales**

**FACTURACIÓN:**

**Archivo**| Descripción del archivo
------- | -------- 
Pago exitoso   | Email que se enviara al completar el pago 
Pago rechazado | Email que se enviara al rechazar el pago  
Próximo pago   | Email que notifica la fecha de otro pago  

**NOTIFICACIONES:**

- Aviso de últimos dias de prueba gratis     

- Fin de la prueba y tener que pagar a huevo 

#### Script hecho por:
**Marco Antonio Castillo Reyes 8)**