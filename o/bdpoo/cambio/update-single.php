<?php

/**
 * Use an HTML form to edit an entry in the
 * users table.
 *
 */

require "config.php";
require "common.php";

if (isset($_POST['submit'])) {
  //if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $connection = new PDO($dsn, $username, $password, $options);

    $user =[
      "mail"        => $_POST['mail'],
      "nombre" => $_POST['nombre'],
      "pass"  => $_POST['pass']
    ];

    $sql = "UPDATE clientes 
            SET mail = :mail, 
              nombre = :nombre, 
              pass = :pass 
            WHERE mail = :mail";
  
  $statement = $connection->prepare($sql);
  $statement->execute($user);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}
  
if (isset($_GET['id'])) {
  try {
    $connection = new PDO($dsn, $username, $password, $options);
    $id = $_GET['id'];

    $sql = "SELECT * FROM clientes WHERE mail = :mail";
    $statement = $connection->prepare($sql);
    $statement->bindValue(':mail', $id);
    $statement->execute();
    
    $user = $statement->fetch(PDO::FETCH_ASSOC);
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
} else {
    echo "Something went wrong!";
    exit;
}
?>

<?php if (isset($_POST['submit']) && $statement) : ?>
	<blockquote><?php echo escape($_POST['mail']); ?> successfully updated.</blockquote>
<?php endif; ?>

<h2>Editar cliente</h2>

<form method="post">
    <!--<input name="csrf" type="hidden" value="<?php //echo escape($_SESSION['csrf']); ?>">-->
    <?php foreach ($user as $key => $value) : ?>
      <label for="<?php echo $key; ?>"><?php echo ucfirst($key); ?></label>
	    <input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo escape($value); ?>" <?php echo ($key === 'mail' ? 'readonly' : null); ?>>
    <?php endforeach; ?> 
    <input type="submit" name="submit" value="Cambiar">
</form>

<a href="../index.php">Regresar al Inicio</a>
