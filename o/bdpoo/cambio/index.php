<?php

/**
 * List all users with a link to edit
 */

require "config.php";
require "common.php";

try {
  $connection = new PDO($dsn, $username, $password, $options);

  $sql = "SELECT * FROM clientes";

  $statement = $connection->prepare($sql);
  $statement->execute();

  $result = $statement->fetchAll();
} catch(PDOException $error) {
  echo $sql . "<br>" . $error->getMessage();
}
?>
<h2>Actualizar usuario</h2>

<table>
    <thead>
        <tr>
            <th>Email</th>
            <th>Nombre</th>
            <th>Contraseña</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($result as $row) : ?>
        <tr>
            <td><?php echo escape($row["mail"]); ?></td>
            <td><?php echo escape($row["nombre"]); ?></td>
            <td><?php echo escape($row["pass"]); ?></td>
            <td><a href="update-single.php?id=<?php echo escape($row["mail"]); ?>">Editar</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<a href="../index.php">Regresar al Inicio</a>
