<?php

/**
 * Function to query information based on 
 * a parameter: in this case, location.
 *
 */

require "config.php";
require "common.php";

if (isset($_POST['submit'])) {
  //if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try  {
    $connection = new PDO($dsn, $username, $password, $options);

    $sql = "SELECT * 
            FROM clientes
            WHERE mail = :location";

    $location = $_POST['location'];
    $statement = $connection->prepare($sql);
    $statement->bindParam(':location', $location, PDO::PARAM_STR);
    $statement->execute();

    $result = $statement->fetchAll();
  } catch(PDOException $error) {
      echo $sql . "<br>" . $error->getMessage();
  }
}
?>
        
<?php  
if (isset($_POST['submit'])) {
  if ($result && $statement->rowCount() > 0) { ?>
    
      <?php foreach ($result as $row) : ?>        
          <?php echo escape($row["nombre"]); ?>
          <br><?php echo escape($row["mail"]); ?>
          <br><?php echo escape($row["pass"]); ?>        
      <?php endforeach; ?>
    <?php } else { ?>
      <h1>No results found for <?php echo escape($_POST['location']); ?>.</h1>
    <?php } 
} ?> 
