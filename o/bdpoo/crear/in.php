<?php
/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */
//REQUIERE ARCHIVOS EXTERNOS
require "config.php";
require "common.php";

//PROCESO AL RECIBIR LOS DATOS DEL FORMULARIO
if (isset($dos->tic)) {
  //if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try  {
    //CONECTA CON LA BASE DE DATOS
    $connection = new PDO($dsn, $username, $password, $options);
    //RECIBE LOS DATOS DEL FORMULARIO EN UN ARREGLO
    $new_user = array(
      "nombre" => $dos->alias,
      "mail"  => $dos->carta,
      "pass"     => $dos->llave
    );
    //CREA LA INSTRUCCIÓN SQL PARA GUARDAR EN LA BD
    $sql = sprintf(
      "INSERT INTO %s (%s) values (%s)",
      "clientes",
      implode(", ", array_keys($new_user)),
      ":" . implode(", :", array_keys($new_user))
    );
    //PREPARA LA SENTENCIA
    $statement = $connection->prepare($sql);
    //EJECUTA LA SENTENCIA
    $statement->execute($new_user);
  } catch(PDOException $error) {
      //MUESTRA LOS ERRORES
      echo $sql . "<br>" . $error->getMessage();
  }
}
?>

  <?php if (isset($dos->tic) && $statement) : $dos->toc=true; endif; ?>
<?php
if ($dos->toc==true) {
  echo "El cliente fue dado de alta.<br>";
}
else
{
  header('Location: indez.php');
}
?>