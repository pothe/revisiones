<?php

/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */
//REQUIERE ARCHIVOS EXTERNOS
require "config.php";
require "common.php";

//PROCESO AL RECIBIR LOS DATOS DEL FORMULARIO
if (isset($_POST['submit'])) {
  //if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try  {
    //CONECTA CON LA BASE DE DATOS
    $connection = new PDO($dsn, $username, $password, $options);
    //RECIBE LOS DATOS DEL FORMULARIO EN UN ARREGLO
    $new_user = array(
      "nombre" => $_POST['nombre'],
      "mail"  => $_POST['mail'],
      "pass"     => $_POST['pass']
    );
    //CREA LA INSTRUCCIÓN SQL PARA GUARDAR EN LA BD
    $sql = sprintf(
      "INSERT INTO %s (%s) values (%s)",
      "clientes",
      implode(", ", array_keys($new_user)),
      ":" . implode(", :", array_keys($new_user))
    );
    //PREPARA LA SENTENCIA
    $statement = $connection->prepare($sql);
    //EJECUTA LA SENTENCIA
    $statement->execute($new_user);
  } catch(PDOException $error) {
      //MUESTRA LOS ERRORES
      echo $sql . "<br>" . $error->getMessage();
  }
}
?>

  <?php if (isset($_POST['submit']) && $statement) : ?>
    <blockquote><?php echo escape($_POST['nombre']); ?> successfully added.</blockquote>
  <?php endif; ?>
<center>
  <h2>Agregar Cliente</h2>

  <form method="post">
    <!--<input name="csrf" type="hidden" value="<?php //echo escape($_SESSION['csrf']); ?>">-->
    <label>Nombre</label>
    <input type="text" name="nombre"><br>
    <label>Correo Electrónico</label>
    <input type="text" name="mail"><br>
    <label>Contraseña</label>
    <input type="password" name="pass"><br>
    <input type="submit" value="Ingresar" name="submit">
</form>
</center>
  <a href="../index.php">Regresar al Inicio</a>
