# Sistema SaaS en Orientado a Objetos

**Script para implementar un SaaS**

## Este script contiene desde el registro, renovación, pago, recuperación de contraseña e ingreso a la plataforma del SaaS.

### Carpeta BDPOO

**ARCHIVOS DE BORRAR:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo con formulario para ingresar y borrar datos         
config.php      | Archivo con datos de conexión a la BD                
common.php     | Archivo que contiene una función de escape

**ARCHIVOS DE CAMBIO:**

**Archivo**| Descripción del archivo 
------ | -------- 
index.php      | Archivo que despliega los datos de la BD y con un botón se eliminan         
config.php      | Archivo con datos de conexión a la BD                
common.php     | Archivo que contiene una función de escape
update-single.php | Archivo que borra los datos seleccionados en index.php

**ARCHIVOS DE VER:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo con formulario para ingresar         
config.php      | Archivo con datos de conexión a la BD                
common.php     | Archivo que contiene una función de escape
ver.php       | Archivo que recibe datos y confirma para dar el acceso

**ARCHIVOS DE CREAR:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo con formulario para ingresar y guardar datos en la BD         
config.php      | Archivo con datos de conexión a la BD                
common.php     | Archivo que contiene una función de escape
i.php 		   | Archivo como el index.php pero desacoplado
alta.php 	   | Archivo que recibe los datos de i.php
in.php 		   | Archivo con script para guardar en la BD con PDO
r.php 		   | Archivo con clases para sesiones y recibir datos de i.php

### Otros Scripts


**ARCHIVOS DE SMTP:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo con formulario para ingresar datos         
e.php      | Archivo con las clases para enviar email                
e1.php     | Archivo que recibe datos y lo envia a enviar.php
enviar.php | Archivo que envia el correo electrónico con SMTP
otro.php   | Archivo con formulario para enviar con otro SMTP
otro1.php  | Archivo que envia lo recibido del archivo otro.php
em.php     | Archivo con claves para enviar con EM

**ARCHIVOS DE RENOVACIÓN:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo con formulario para ingresar renovación         
a.php      | Archivo con las clases para renovar                
a1.php     | Archivo que recibe datos a renovar
a2.php     | Archivo sin definir
a3.php     | Archivo de confirmación
ta.php     | Archivo para datos de tarjeta de crédito
card.js    | Archivo JS de la tarjeta de crédito

**ARCHIVOS DE REGISTRO:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo con formulario del registro            
r.php      | Archivo con la clase para registrar                
regi.php     | Archivo para confirmar datos ingresados
regis.php | Archivo que guarda los datos en la base de datos
mail.php | Archivo para enviar correos (Falta integrarlo)

**ARCHIVOS DE FECHADOR:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo que llama las clases
f.php      | Archivo con la clase para las fechas

**ARCHIVOS DE INGRESO:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php| Archivo con formulario de acceso
i.php  | Archivo con clases de ingreso
i1.php | Archivo del tablero del SaaS 
i2.php | Archivo que destruye la sesión 
i3.php | Archivo para modificar datos de usuario 
mod.php| Archivo que actualiza la base de datos
**(FALTA)ARCHIVOS DE RECUPERACIÓN:**

**Archivo**| Descripción del archivo
------ | -------- 
rec.html      | Archivo con formulario para solicitar contraseña 
recu.php      | Archivo que muestra mensaje y envia el email     
recumail.php  | Archivo con la plantilla del email a enviar      


### Se debe de cambiar los datos de acceso a la base de datos e importar archivo "basedatos.sql" (Archivo .sql maestro aún sin definir)

## SCRIPTS FALTANTES:
**Falta desarrollar estos scripts para el SaaS**

### Scripts de Facturación con SMTP:
- [ ] Pago exitoso - Email que se enviara al completar el pago 
- [ ] Pago rechazado - Email que se enviara al rechazar el pago 
- [ ] Próximo pago- Email que notifica la fecha de otro pago 

### Scripts de Recuperación con STMP:
- [ ] Envio de email con confirmación para enviar el email con las credenciales.

### Scripts de Notificaciones en Panel de Control:
- [ ] Script de Aviso de últimos dias de prueba gratis
- [ ] Script de Fin de la prueba y tener que pagar a huevo tipo Hyper.host


### Script para actualizar (ping) y enviar emails de alerta (SMTP)
- [X] Hacer listado de emails a los que se han utilizado para realizar pruebas de envió
- [ ] Agregar estos correos a la BD
- [ ] Desarrollar script que con un ping se realiza un ciclo  que extrae el email y nombre, estos datos de guardarán en un objeto y en una plantilla de email ahí se pondrán los datos del objeto para después enviar el email con SMTP
- [ ] **El script anterior pero extraerá la fecha la cúal se hará una comparación y enviar el email de los días que le quedan o si ya expiró.**

#### Script hecho por:
**Marco Antonio Castillo Reyes 8)**