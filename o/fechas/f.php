<?php
//CLASE PARA GENERAR Y GUARDAR FECHAS
class Fecha{
	//DECLARACION DE ATRIBUTOS
	private $inicio;
	private $inter;
	private $arfe;
	private $arfa;
	private $basu;
	private $ason;
	public $eson;

	//METODO PARA EXTRAER LA FECHA DE HOY
	private function Hoy()
	{
		$this->inicio = date('d/m/Y');
	}//Fin de hoy

	//METODO PARA GENERAR 7 FECHAS POSTERIORES
	private function SieteDias()
	{
		//Arreglo para guardar 7 fechas
		$this->arfe=array();

		//Arreglo para ir guardando fecha tras fecha que a su vez será guardada en el arreglo $arfe
		$this->arfa=array();

		//Ciclo que genera fechas y las guarda en un arreglo, mismas que se guardan en el arreglo $arfe
		for ($i=0; $i<8 ; $i++) { 
			
		  //$arfe[$i]=date('d/m/Y',strtotime('+'.$i.' days'));
		  //Se genera una fecha y se guarda en la variable $basu
		  //$basu=date('d/m/Y',strtotime('+'.$i.' days'));
		  $this->basu=date('d/m/Y',strtotime('-'.$i.' days,+1 month'));
		  //Se guarda la fecha asignandole un número
		  $this->arfa=array(
				'No'=>$i,
				'Fecha'=>$this->basu
			);

		  	array_unshift($this->arfe,$this->arfa);

		  	//La fecha guardada en un arreglo se guarda dentro del arreglo $arfe
		  //array_push($arfe,$arfa);
		}

		//El arreglo $arfe se convierte a JSON y se guarda en $ason
		echo $this->ason= serialize($this->arfe);
		
	}//Fin del método

	//METODO PARA GENERAR 3 FECHAS POSTERIORES
	private function TresDias()
	{
		//Arreglo para guardar 3 fechas
		$this->arfe=array();

		//Arreglo para ir guardando fecha tras fecha que a su vez será guardada en el arreglo $arfe
		$this->arfa=array();

		//Ciclo que genera fechas y las guarda en un arreglo, mismas que se guardan en el arreglo $arfe
		for ($i=0; $i<3 ; $i++) { 
			
		  //$arfe[$i]=date('d/m/Y',strtotime('+'.$i.' days'));
		  //Se genera una fecha y se guarda en la variable $basu
		  //$basu=date('d/m/Y',strtotime('+'.$i.' days'));
			switch ($i) {
				case 0:
					$this->basu=date('d/m/Y');
					break;
				case 1:
					$this->basu=date('d/m/Y',strtotime('+15 days'));
					break;
				case 2:
					$this->basu=date('d/m/Y',strtotime('+1 month'));
					break;
			}//Fin del switch

		  //$this->basu=date('d/m/Y',strtotime('-'.$i.' days,+1 month'));
		  //Se guarda la fecha asignandole un número
		  $this->arfa=array(
				'No'=>$i,
				'Fecha'=>$this->basu
			);

		  	array_unshift($this->arfe,$this->arfa);

		  	//La fecha guardada en un arreglo se guarda dentro del arreglo $arfe
		  //array_push($arfe,$arfa);
		}//Fin del for

		//El arreglo $arfe se convierte a JSON y se guarda en $ason
		echo $this->inter= serialize($this->arfe);
	}//Fin del método

	//METODO PARA VISUALIZAR LAS 7 FECHAS GENERADAS
	public function VerSiete()
	{
		echo "<br>";
		//Función para ver las 7 fechas convertidas en JSON
		print_r($this->eson = unserialize($this->ason));
		//Imprimir una fecha del array JSON
		echo "<br>".$this->eson[1]['Fecha'];
	}//Fin de VerSiete

	//MÉTODO QUE GUARDA LAS FECHAS EN BASE DE DATOS LOCAL
	private function Guardecha($postal,$trio,$seven)
	{
		$conexion=mysqli_connect("localhost","root","Soledad1995","wpex") or
		    die("Problemas con la conexión");

		mysqli_query($conexion,"insert into limite(postal,tres,siete) values 
		                       ('$postal','$trio','$seven')")
		  or die("Problemas en el select".mysqli_error($conexion));

		mysqli_close($conexion);
	}//Fin del método

	//METODO MAESTRO QUE GENERA Y GUARDA LAS FECHAS CON DEMAS METODOS
	public function LlamadaInterna()
	{
		//Email que será la llave primaria
		$mail="arco@nuve.ml";

		//Llamada el método para extraer la fecha de hoy
		$this->Hoy();

		//Llamada de método que genera 7 dias seguidos
		echo "7 Dias: <br>";
		$this->SieteDias();

		echo "<br>3 Dias: <br>";
		//LLamada de método que genera 3 dias
		$this->TresDias();

		//Se envian a la BD las fechas a guardar
		$this->Guardecha($mail,$this->inter,$this->ason);
	}//Fin del método

}//Fin de la clase

//ESTA CLASE ES PARA EXTRAER LAS FECHAS GUARDADAS EN LA BD E IMPRIMIRLAS
class Alertas{
	public $comienzo;
	public $seven;
	public $three;
	public $dias;
	public $tercio;
	public $clienta;
	public $ban;

	//METODO PARA EXTRAER FECHAS DE LA LLAVE PRIMARIA ASIGNADA
	public function VerAlertas()
	{
		//LLave primaria de la que se extraeran las fechas
		if (empty($ban)) {
			$this->comienzo='oye@pothe.gq';
		}
		else
		{//Fin del if
			$this->comienzo=$this->$clienta;
		}//Fin del else

		$conexion=mysqli_connect("localhost","root","Soledad1995","wpex") or
		    die("Problemas con la conexión");

		$registros=mysqli_query($conexion,"select postal,tres,siete
		                        from limite where postal='$this->comienzo'") or
		  die("Problemas en el select:".mysqli_error($conexion));

			if ($reg=mysqli_fetch_array($registros))
		{
			$this->three=$reg['tres'].'<br>';
			$this->seven=$reg['siete'].'<br>';
		}//Fin del if
		else
		{
		  echo "Datos Incorrectos Email.";
		}//Fin del else
		mysqli_close($conexion);
	}//Fin del método

	public function VerMensajes($cliente)
	{
		$this->clienta=$cliente;

		$this->VerAlertas();
	}

	//METODO PARA VISUALIZAR LAS 7 FECHAS GENERADAS
	public function VerSeven()
	{
		echo "<br>Viendo 7: <br>";
		//Función para ver las 7 fechas convertidas en JSON
		print_r($this->dias = unserialize($this->seven));
		//Imprimir una fecha del array JSON
		//echo "<br><br>".$this->dias[0]['Fecha'];
		//echo "<br>".$this->dias[1]['Fecha'];
		//echo "<br>".$this->dias[2]['Fecha'];
		for ($i=0; $i < 8; $i++) { 
			echo "<br>".$this->dias[$i]['Fecha'];
		}
	}//Fin de VerSiete

	//METODO PARA VISUALIZAR LAS 3 FECHAS GENERADAS
	public function VerTres()
	{
		echo "<br>Viendo 3: <br>";
		//Función para ver las 3 fechas convertidas en JSON
		print_r($this->tercio = unserialize($this->three));
		//Imprimir una fecha del array JSON
		echo "<br><br>Fecha de Inicio: ".$this->tercio[2]['Fecha'];
		echo "<br>Fecha de Aviso: ".$this->tercio[1]['Fecha'];
		echo "<br>Fecha de Fin: ".$this->tercio[0]['Fecha'];
	}//Fin de VerTres

	//METODO PARA VISUALIZAR LAS 3 FECHAS GENERADAS
	public function Ver_Tres()
	{
		$fin=date('d/m/Y');
		//echo "<br>Viendo 3: <br>";
		//Función para ver las 3 fechas convertidas en JSON
		$this->tercio = unserialize($this->three);
		//Imprimir una fecha del array JSON
		//echo "<br><br>Fecha de Inicio: ".$this->tercio[2]['Fecha'];
		//echo "<br>Fecha de Aviso: ".$this->tercio[1]['Fecha'];
		//echo "<br>Fecha de Fin: ".$this->tercio[0]['Fecha'];

		if ($this->tercio[2]['Fecha']==$fin) {
			echo "<h1>ULTIMO DÍA DE PRUEBA, POR FAVOR CONTRATA</h1>";
		}
		elseif ($this->tercio[1]['Fecha']==$fin) {
			echo "TE QUEDA 1 DÍA DE PRUEBA GRATIS";
		}
		elseif ($this->tercio[0]['Fecha']==$fin) {
			echo "ULTIMO DÍA DE PRUEBA, POR FAVOR CONTRATA";
		}
		else
		{
			//header('Location: i4.php');
			echo "PRUEBA FINALIZADA, POR FAVOR CONTRATA";
		}
	}//Fin de VerTres

}//Fin de la clase

?>