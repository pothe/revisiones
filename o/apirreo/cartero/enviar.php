<?php
//ESTE SCRIPT SI SIRVIO :)
		require 'vendor/autoload.php';

		use PHPMailer\PHPMailer\PHPMailer;
		use PHPMailer\PHPMailer\Exception;
 
		$mail = new PHPMailer;

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = $carta->destino;                     // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $carta->usuario;   // SMTP username
		$mail->Password = $carta->llave;                 // SMTP password
		$mail->Port = $carta->puerto;
		$mail->SMTPSecure = $carta->ttls;

		$mail->From = $carta->remitente;
		$mail->FromName = $carta->nomrem;
		$mail->addAddress($carta->destinatario,$carta->nomdes); // Add a recipient
		$mail->WordWrap = 50;                                 // Set word wrap to 50 characters

		$mail->CharSet = 'UTF-8';
		$mail->isHTML(true);
		$mail->Subject = $carta->titulo;
		$mail->Body    = $carta->cuerpo;
		$mail->AltBody = $carta->puerco;

		if(!$mail->send()) {
			$carta->alerta=true;
		    echo 'El mensaje no se pudo enviar.';
		    echo 'Error: ' . $mail->ErrorInfo;
		} else {
			$carta->alerta=false;
		    echo 'El Mensaje ha sido enviado con éxito';
		}