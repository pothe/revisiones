<?php
//CLASE MAESTRA PARA ENVIAR CON PHPMAILER
class OficinaPostal{
	public $destino;	
	public $usuario;
	public $llave;
	public $puerto;
	public $ttls;

	public $remitente;
	public $destinatario;
	public $nomrem;
	public $nomdes;
	public $titulo;
	public $cuerpo;
	public $puerco;

	public $alerta;

	//CONSTRUCTOR QUE RECIBE LOS DATOS DE LA CLASE 'CARTA'
	public function __construct($sobre,$info)
	{
		$this->remitente=$sobre->remite;
		$this->nomrem=$sobre->noremite;
		$this->destinatario=$sobre->destino;
		$this->nomdes=$sobre->nodestino;
		$this->titulo=$sobre->cabeza;
		$this->cuerpo=$sobre->contenido;
		$this->puerco=$sobre->altexto;
		//LLAMADA DE MÉTODO SERVIDOR
		$this->Servidor($info);
	}//Fin del constructor

	//METODO PARA LLAMAR AL SERVIDOR SMTP PARA ENVIAR
	public function Servidor($gafete)
	{
		$this->destino=$gafete->hosting;
		$this->usuario=$gafete->cliente;
		$this->llave=$gafete->clave;
		$this->puerto=$gafete->porto;
		$this->ttls=$gafete->stl;
	}//Fin de Servidor
}//Fin de clase

//CLASE PARA SELECCIONAR SERVIDOR SMTP A ENVIAR
class Servidores{
	public $hosting;
	public $cliente;
	public $clave;
	public $porto;
	public $slt;

	public function __construct($opcion,$zelda='',$fan='',$santo='',$porta='',$tsl='')
	{
		switch ($opcion) {
			case 'a':
				$this->MailElastico();
				break;
			case 'b':
				$this->PostSpark();
				break;
			case 'c':
				$this->JetCorreo();
				break;			
			default:
				$this->Manual($zelda,$fan,$santo,$porta,$tsl);
				break;
		}//Fin del switch
	}//Fin del método

	//METODO PARA ENVIAR CON ELASTICEMAIL
	public function MailElastico()
	{
		$this->hosting='smtp.elasticemail.com';
		$this->cliente='gestor@adop.top';
		$this->clave='ab014e95-ecb3-4a8f-8d57-3d40d34ce014';
		$this->porto=2525;
	}//Fin de la funcion

	//METODO PARA ENVIAR CON SPARKPOST
	public function PostSpark()
	{
		$this->hosting='smtp.sparkpostmail.com';
		$this->cliente='SMTP_Injection';
		$this->clave='a73b5ea57cb472bf1f71ca8311c5f69bf896ea6c';
		$this->porto=2525;
		$this->slt='STARTTLS';
	}//Fin de la funcion

	//METODO PARA ENVIAR CON MAILJET
	public function JetCorreo()
	{
		$this->hosting='in-v3.mailjet.com';
		$this->cliente='0bdaa413696572f64dafd31252205c99';
		$this->clave='41b2adf253ec09e3a5aa7989f8d2a5d6';
		$this->porto=587;
	}//Fin de la funcion

	//METODO PARA INGRESO MANUAL DE CREDENCIALES
	public function Manual($uno,$dos,$tres,$cuatro,$five)
	{
		$this->hosting=$uno;
		$this->cliente=$dos;
		$this->clave=$tres;
		$this->porto=$cuatro;
		$this->slt=$five;
	}//Fin de la funcion
}//Fin de la clase

//CLASE QUE RECIBE LOS DATOS DEL EMAIL A ENVIAR
class Carta{
	public $remite;
	public $noremite;
	public $destino;
	public $nodestino;
	public $cabeza;
	public $contenido;
	public $altexto;

	//METODO QUE CREA EL OBJETO CON LOS DATOS
	public function __construct($envia,$noenv,$recibe,$norec,$titulo,$recado,$texto)
	{
		$this->remite=$envia;
		$this->noremite=$noenv;
		$this->destino=$recibe;
		$this->nodestino=$norec;
		$this->cabeza=$titulo;
		$this->contenido=$recado;
		$this->altexto=$texto;
	}//Fin del constructor
}//Fin de clase

?>