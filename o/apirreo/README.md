# APItor - API & Creator (Posteador y Receptor)

**Script para realizar un POST y recibirlo**

Este script es para las bases de creación de una API con el manejo de POST.

## Archivos de Posteador:
Archivo | Descripción de archivo
---- | ----
index.html | HTML con formulario para enviar a via.php
*__via.php__*  | *__PHP que realiza el POST__*
`curl.php`  | `PHP de ejemplo para realizar POST con más campos`

## Archivos de Receptor:

**Archivo** | Descripción de archivo 
---- | ----
index.html | HTML con formulario para enviar a va.php
*__va.php__*  | *__PHP que recibe el POST__*
_ve.php_  | _PHP para observar los datos recibidos y guardar en a.json_
a.json  | JSON que guarda los datos 

## Archivos de Apitest:
Archivo | Descripción de archivo
---- | ----
index.php | Formulario a enviar
*__manda.php__*  | *__PHP que realiza el POST__*
*__mandar.php__*  | *__PHP que realiza el GET__*
`ping.php`  | `PHP que realiza un ping con objetos`

## Falta hacer:
- [x] Ver como funciona el POST
- [ ] Hacer un script más complejo para el APItor
- [ ] \(Opcional) Hacerlo módulo para implementarlo con otro scripts


Script PHP desarrollado por [Marco Antonio Castillo Reyes](https://bitbucket.org/pothe/)