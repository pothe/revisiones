<?php
//Script que realiza un ping a una URL definida

//Declaración de clase
class ping {
	//Declaración de atributos
	private $url;

	//Declaración de método
	//Función con constructor y parametro que recibe la URL
	public function __construct($link)
	{
		$url = $link;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if($httpcode>=200 && $httpcode<300){
		  echo 'Exitoso';
		} else {
		  echo "Sigue participando :(";
		}
	}//Fin de recibeLink
}//Fin de ping

//Declaración de objetos con constructor y envió de parametro
$uno=new ping('http://xert.cf/g.php?l=a');
?>