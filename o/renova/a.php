<?php
class Renovar{
	public $forma;
	public $billete;

	public function __construct($formato,$dinero)
	{
		$this->forma=$dinero;
		$this->billete=$formato;
	}//Fin del constructor	
	
}//Fin de clase

class Renovando{
	private $proceso;
	private $ruta;

	public function __construct($dato)
	{
		$this->proceso=$dato;

		$this->Enrutador();
	}//Fin del constructor

	//Método para redirigir a pagar
	private function Enrutador()
	{
		//Si pagará con tarjeta de crédito
		if ($this->proceso->forma=='tarja') {			  			
			switch ($this->proceso->billete) {
				case 'a':
					header('Location: ta.php?c=2');
					break;
				case 'b':
					header('Location: ta.php?c=4');
					break;
				case 'c':
						header('Location: ta.php?c=6');
					break;	
			}//Fin del switch
		}//Fin del if
		//Si pagará con Mercado Pago
		elseif ($this->proceso->billete=='mepa') {
			echo "Pagar con MercadoPago";
		}
		//Si pagará con PayPal
		else
		{
			header('Location: index.php');
		}//Fin del else
	}//Fin del enrutador
}//Fin de clase

class Cantidad{
	public $plazo;
	public $precio;

	public function __construct($val)
	{
		$this->plazo=$val;

		switch ($this->plazo) {
			case '2':
				$this->precio='$2 USD';
				break;
			case '4':
				$this->precio='$4 USD';
				break;
			case '6':
				$this->precio='$6 USD';
				break;
		}//Fin del switch
	}//Fin del constructor
}//Fin de la clase

?>