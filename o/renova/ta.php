﻿<?php
require 'a.php';
$credito=new Cantidad($_GET['c']);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Realizar pago con tarjeta de crédito</title>
    <meta name="viewport" content="initial-scale=1">
    <!-- CSS is included through the card.js script -->
</head>
<body>
    <style>
        .demo-container {
            width: 100%;
            max-width: 350px;
            margin: 50px auto;
        }

        form {
            margin: 30px;
        }
        input {
            width: 200px;
            margin: 10px auto;
            display: block;
        }

    </style>
    <center><h1>Pago de <?php echo $credito->precio; ?></h1></center>
    <div class="demo-container">        
        <div class="card-wrapper"></div>

        <div class="form-container active">            
            <form action="a3.php" method="post">
                <input placeholder="Número de Tarjeta" type="tel" name="number">
                <input placeholder="Nombre en la Tarjeta" type="text" name="name">
                <input placeholder="MM/YY" type="tel" name="expiry">
                <input placeholder="CVC" type="number" name="cvc">
                <input type="submit" value="Pagar">
            </form>
        </div>
    </div>

    <script src="card.js"></script>
    <script>
        new Card({
            form: document.querySelector('form'),
            container: '.card-wrapper'
        });
    </script>
</body>
</html>
