<?php
class PagoExitoso{
	private $Folio;
	private $Paquete;
	private $Cantidad;
	private $Medio;

	public function __construct($id,$pack,$plata,$forma)
	{
		$this->Folio=$id;
		$this->Paquete=$pack;
		$this->Cantidad=$plata;
		$this->Medio=$forma;		
	}//Fin de constructor

	public function Mirar()
	{
		echo "<p>Folio: ".$this->Folio."</p>";
		echo "<p>Paquete: ".$this->Paquete."</p>";
		echo "<p>Cantidad: ".$this->Cantidad."</p>";
		echo "<p>Medio de Pago: ".$this->Medio."</p>";
	}//Fin de Mirar
}//Fin de clase

$caja=new PagoExitoso(23,'1 Mes',2,'Tarjeta de Cŕedito')

?>

<!DOCTYPE html>
<html>
<head>
	<title>Pago Exitoso</title>
</head>
<body>
<center>
	<h1>Felicidades tu SaaS ha sido Renovado</h1>
	<?php $caja->Mirar(); ?>

	<br><a href="index.php">Regresar al Panel de Usuario</a>
</center>
</body>
</html>