# Cartero 

## Script para enviar emails via SMTP con PHPMailer

**Este script es para enviar emails con servidores SMTP**

**ENVIO CON CREDENCIALES:**

**Archivo**| Descripción del archivo
------ | -------- 
index.php      | Archivo con formulario para ingresar datos         
e.php      | Archivo con las clases para enviar email                
e1.php     | Archivo que recibe datos y lo envia a enviar.php
em.php     | Archivo con claves para enviar con EM
enviar.php | Archivo que envia el correo electrónico con SMTP

**ENVIO CON OTRO SMTP**

**Archivo**| Descripción del archivo
------ | -------- 
e.php      | Archivo con las clases para enviar email                
otro.php   | Archivo con formulario para enviar con otro SMTP
otro1.php  | Archivo que envia lo recibido del archivo otro.php
otro2.php  | Archivo que recibe con método GET
enviar.php | Archivo que envia el correo electrónico con SMTP